#ifndef __FF_HEADER_GAME
#define __FF_HEADER_GAME

#include <string>
#include <cstdlib>
#include <ctime>
#include <list>
#include <limits>
#include <iostream>
#include <chrono>

#include <raylib.h>
#include <raymath.h>
#include "engine/raylib_operators.hpp"

#include "game_timestamps.hpp"
#include "stage/stage.hpp"

#include "entity/player.hpp"
#include "entity/enemy.hpp"
#include "entity/enemy_customer.hpp"
#include "entity/enemy_karen.hpp"

#include "weapon/weapon.hpp"
#include "weapon/weapon_rice.hpp"
#include "weapon/weapon_corn.hpp"
#include "weapon/weapon_cookie.hpp"

#include "engine/assets.hpp"


enum GameEndCondition
{
    PLAYER_DEAD,
};

struct GameState
{
    bool is_running = false;

    GameAssets assets;
    Camera2D camera;

    // stage stuff
    Stage stage;

    // timing stuff
    GameTimestamps timestamps;

    // player stuff
    Player player;

    // enemy stuff
    std::list<Enemy> enemies;
    unsigned int max_enemy_count = 50;
    Enemy* closest_enemy;
    float closest_enemy_distance;
    float enemy_despawn_distance = 1500;

    // constructor
    GameState();
};

// Run the game with a given state
void RunGame(GameState& state);
void EndGame(GameState& state, GameEndCondition condition);

// Render the entire state to the screen
void UpdateGame(GameState& state);
void UpdateCamera(GameState& state);

void UpdatePlayer(GameState& state);
void UpdateExp(GameState& state, int amount);

void UpdateStage(GameState& state);

void UpdateEnemies(GameState& state);
bool DamageEnemy(GameState& state, std::list<Enemy>::iterator& enemy_iterator, float damage);
void SpawnEnemy(GameState& state, EnemyType type);

void UpdateWeapons(GameState& state);

void RenderGameState(GameState& state);
void RenderStage(GameState& state);
void RenderPlayer(GameState& state);
void RenderEnemies(GameState& state);

void RenderWeapons(GameState& state);
void RenderWeaponRice(GameState& state, Weapon& weapon);
void RenderWeaponCorn(GameState& state, Weapon& weapon);
void RenderWeaponCookie(GameState& state, Weapon& weapon);

void RenderUI(GameState& state);
void RenderDebugInfo(GameState& state);

inline Vector2 Vector2ChangeLength(Vector2 v, float scale)
{
    // new length divided by magnitude of vector
    v = Vector2Normalize(v);
    float LdivMagV = scale / (v.x * v.x + v.y * v.y);
    // scale the resulting vector
    Vector2 res {
        v.x * LdivMagV,
        v.y * LdivMagV,
    };
    return res;
}

#endif
