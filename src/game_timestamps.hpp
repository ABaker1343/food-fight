#ifndef __FF_HEADER_GAME_TIMESTAMPS
#define __FF_HEADER_GAME_TIMESTAMPS

#include <chrono>

struct GameTimestamps
{
    std::chrono::time_point<std::chrono::system_clock> last_update;
    std::chrono::time_point<std::chrono::system_clock> this_update;
    std::chrono::milliseconds time_delta;
};

inline bool CheckCooldown(std::chrono::time_point<std::chrono::system_clock> time_now, std::chrono::time_point<std::chrono::system_clock> cooldown_timestamp, std::chrono::milliseconds cooldown)
{
    if (std::chrono::duration_cast<std::chrono::milliseconds>(time_now - cooldown_timestamp) >= cooldown)
        return true;
    
    return false;
}

#endif
