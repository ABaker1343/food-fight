#include "game.hpp"
#include <raylib.h>

int main()
{
    InitWindow(1280, 720, "Food Fight");
    SetTargetFPS(300);

    // run game in its own scope so that the raylib instance is not destroyed before
    // assests for game are unloaded as the unloading is done in the destructors
    {
        GameState state = GameState();

        RunGame(state);
    }

    CloseWindow();
}
