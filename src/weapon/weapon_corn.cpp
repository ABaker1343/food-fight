#include "weapon_corn.hpp"

Weapon CreateWeaponCorn()
{
    Weapon new_weapon;
    new_weapon.type = WeaponType::CORN;
    new_weapon.timestamp_last_cooldown = std::chrono::system_clock::now();

    new_weapon.damage = 200;
    new_weapon.size = {20, 300};
    new_weapon.speed = 800.f / 1000;
    new_weapon.cooldown = std::chrono::milliseconds(500);
    
    new_weapon.weapon = new WeaponCorn();

    return new_weapon;
}

WeaponCorn::WeaponCorn()
{
    this->max_seed_count = 4;
    this->seeds = std::list<Seed>();
    this->expansion_time = std::chrono::milliseconds(700);
}

WeaponCorn::Seed::Seed(Vector2 position, std::chrono::time_point<std::chrono::system_clock> time_now)
{
    this->position = position;
    this->timestamp_spawn = time_now;
    this->is_expanded = false;

    this->current_length = 0;
    this->hboxes = std::vector<CollisionBox>(5);
}

Vector2 GetWeaponHitCorn(Weapon& weapon, Enemy& enemy)
{
    WeaponCorn* corn = static_cast<WeaponCorn*>(weapon.weapon);

    //std::array<Rectangle, 2> hitboxes;

    for (auto& seed : corn->seeds)
    {
        if (!seed.is_expanded)
            continue;

        // construct a hitbox for each seed
        float angle = 0;
        Vector2 up = {0, 1};
        Vector2 up_perpendicular = {1, 0};
        Vector2 origin {
            seed.position.x + weapon.size.x,
            seed.position.y + weapon.size.x,
        };

        for (auto& box : seed.hboxes)
        {
            //Vector2 rotated_up = Vector2Rotate(up, angle);
            //Vector2 seed_vector = Vector2Scale(rotated_up, seed.current_length);
            //Vector2 seed_vector_perp = Vector2Rotate(up_perpendicular, angle);

            Vector2 scaled_up = Vector2Scale(up, seed.current_length);
            Vector2 scaled_up_perp = Vector2Scale(up_perpendicular, weapon.size.x);
            Vector2 seed_vector = Vector2Rotate(scaled_up, angle);
            Vector2 seed_vector_perp = Vector2Rotate(scaled_up_perp, angle);
            
            box.tl = seed.position;
            box.tr = Vector2Add(seed.position, seed_vector_perp);
            box.bl = Vector2Add(seed.position, seed_vector);
            box.br = Vector2Add(box.bl, seed_vector_perp);

            if (CheckCollision(box, enemy))
            {
                return seed_vector;
            }

            angle += (360.f / seed.hboxes.size())* DEG2RAD;
        }

    }

    return {0, 0};
}

void UpdateWeaponCorn(Weapon& weapon, const Vector2 player_position, const GameTimestamps& timestamps)
{
    WeaponCorn* corn = static_cast<WeaponCorn*>(weapon.weapon);

    // check if we can spawn another seed
    if (
        corn->seeds.size() < corn->max_seed_count &&
        CheckCooldown(timestamps.this_update, weapon.timestamp_last_cooldown, weapon.cooldown)
    )
    {
        corn->seeds.emplace_back(WeaponCorn::Seed(player_position, timestamps.this_update));
        weapon.timestamp_last_cooldown = timestamps.this_update;
    }

    // check if the seeds can explode
    std::list<WeaponCorn::Seed>::iterator seed_iter;
    for (seed_iter = corn->seeds.begin(); seed_iter != corn->seeds.end(); seed_iter++)
    {
        // check if the seed expanded, if no try and expand
        if (!seed_iter->is_expanded) 
        {
            if (CheckCooldown(timestamps.this_update, seed_iter->timestamp_spawn, corn->expansion_time))
            {
                seed_iter->is_expanded = true;
                seed_iter->timestamp_spawn = timestamps.this_update;
            }
        } else // seed is already expanded
        {
            // try and grow the length
            if (seed_iter->current_length < weapon.size.y) {
                seed_iter->current_length += weapon.speed * timestamps.time_delta.count();
            }
        }

        // check if we have to destroy the seed
        if (seed_iter->current_length > weapon.size.y)
        {
            seed_iter = corn->seeds.erase(seed_iter);
        }
    }
}
