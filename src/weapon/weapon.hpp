#ifndef __FF_HEADER_WEAPON
#define __FF_HEADER_WEAPON

#include <cstdlib>
#include <vector>
#include <array>
#include <stdexcept>
#include <list>
#include <iostream>
#include <raylib.h>
#include <raymath.h>

#include "../entity/enemy.hpp"

struct CollisionBox
{
    Vector2 bl;
    Vector2 br;
    Vector2 tl;
    Vector2 tr;
};

enum WeaponType
{
    RICE, // MACHINE GUN
    BANANNA, // BOOMERANG;
    CORN, // PLANTING WEAPON
    COOKIE, // AXE MELEE WEAPON
};

// forward declare all weapon types
struct WeaponRice;
struct WeaponCorn;
struct WeaponCookie;

struct Weapon
{
    WeaponType type;

    float damage;
    Vector2 size;
    float speed;
    std::chrono::time_point<std::chrono::system_clock> timestamp_last_cooldown;
    std::chrono::milliseconds cooldown;

    Rectangle hitbox;

    void* weapon = nullptr;

    Weapon();
    ~Weapon();
    Weapon(Weapon&) = delete;
    Weapon(Weapon&&);
};


bool CheckCollision(CollisionBox& box, Enemy& enemy);

inline float ProjectToAxis(Vector2 point, Vector2 axis)
{
    return Vector2DotProduct(axis, point);
}

#endif
