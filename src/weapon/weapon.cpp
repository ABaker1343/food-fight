#include "weapon.hpp"
#include <raylib.h>
#include <raymath.h>

Weapon::Weapon() {
    this->timestamp_last_cooldown = std::chrono::system_clock::now();
}

Weapon::~Weapon()
{
    if (this->weapon != nullptr)
    {
        std::free(this->weapon);
    }
}

Weapon::Weapon(Weapon&& src)
{
    this->type = src.type;

    this->damage = src.damage;
    this->size = src.size;
    this->speed = src.speed;
    this->timestamp_last_cooldown = src.timestamp_last_cooldown;
    this->cooldown = src.cooldown;

    this->hitbox = src.hitbox;

    this->weapon = src.weapon;
    
    // transfer pointer ownership
    src.weapon = nullptr;
}

bool CheckCollision(CollisionBox& seed_box, Enemy& enemy)
{
    std::vector<Vector2> axies;
    axies.reserve(4);
    // get the normals of the edges of the seed box
    axies.push_back(Vector2Normalize(Vector2Subtract(seed_box.tl, seed_box.bl)));
    axies.push_back(Vector2Normalize(Vector2Subtract(seed_box.tr, seed_box.tl)));
    // get the normals for the enemy hurtbox (always aligned with up and right)
    axies.push_back({0, 1});
    axies.push_back({1, 0});

    // for each axis project the points of the two boxes to that axis
    // check if they collide on that 1d plane
    // if any plane has no collision then they are separated
    
    for (const auto& axis : axies)
    {
        // get the max point for the seed box on the axis
        float max_point_box = std::max(
            ProjectToAxis(seed_box.bl, axis),
            std::max(
                ProjectToAxis(seed_box.tl, axis),
                std::max(
                    ProjectToAxis(seed_box.tr, axis),
                    ProjectToAxis(seed_box.br, axis)
                )
            )
        );

        // get the min point of the box on the axis
        float min_point_box = std::min(
            ProjectToAxis(seed_box.bl, axis),
            std::min(
                ProjectToAxis(seed_box.tl, axis),
                std::min(
                    ProjectToAxis(seed_box.tr, axis),
                    ProjectToAxis(seed_box.br, axis)
                )
            )
        );

        // get the max point for the enemy on the axis
        float max_point_enemy = std::max(
            ProjectToAxis({enemy.hurtbox.x + enemy.hurtbox.width, enemy.hurtbox.y + enemy.hurtbox.height}, axis),
            std::max(
                ProjectToAxis({enemy.hurtbox.x + enemy.hurtbox.width, enemy.hurtbox.y}, axis),
                std::max(
                    ProjectToAxis({enemy.hurtbox.x, enemy.hurtbox.y + enemy.hurtbox.height}, axis),
                    ProjectToAxis({enemy.hurtbox.x, enemy.hurtbox.y}, axis)
                )
            )
        );

        // get the min point for the enemy on the axis
        float min_point_enemy = std::min(
            ProjectToAxis({enemy.hurtbox.x + enemy.hurtbox.width, enemy.hurtbox.y + enemy.hurtbox.height}, axis),
            std::min(
                ProjectToAxis({enemy.hurtbox.x + enemy.hurtbox.width, enemy.hurtbox.y}, axis),
                std::min(
                    ProjectToAxis({enemy.hurtbox.x, enemy.hurtbox.y + enemy.hurtbox.height}, axis),
                    ProjectToAxis({enemy.hurtbox.x, enemy.hurtbox.y}, axis)
                )
            )
        );

        // check if they overlap
        if (!(max_point_enemy > min_point_box && max_point_box > min_point_enemy)){
            return false;
        }
    }

    // if they are touching on all axis then they are colliding
    return true;

}
