#include "weapon_rice.hpp"

Weapon CreateWeaponRice()
{
    Weapon new_weapon;
    new_weapon.type = WeaponType::RICE;
    new_weapon.timestamp_last_cooldown = std::chrono::system_clock::now();

    new_weapon.damage = 50;
    new_weapon.size = {10, 10};
    new_weapon.speed = 10.f / 1000 * 60;
    new_weapon.cooldown = std::chrono::milliseconds(100);

    new_weapon.hitbox = Rectangle{0, 0, new_weapon. size.x, new_weapon.size.y};

    new_weapon.weapon = new WeaponRice();
    
    return new_weapon;
}

WeaponRice::WeaponRice()
{
    this->max_grains = 5;
    this->grains = std::list<Grain>();
    this->max_grain_distance = 1000;
}

WeaponRice::Grain::Grain(Vector2 starting_position, Vector2 direction)
{
    this->direction = Vector2Normalize(direction);
    this->position = starting_position;

    if (this->direction.x == 0 && this->direction.y == 0)
        this->direction = {0.0, 1.0};
}

bool WeaponRice::Grain::operator==(const WeaponRice::Grain& other)
{
    return  this->position.x == other.position.x &&
            this->position.y == other.position.y &&
            this->direction.x == other.direction.x &&
            this->direction.y == other.direction.y &&
            this->distance_traveled == other.distance_traveled;
}

Vector2 GetWeaponHitRice(Weapon& weapon, Enemy& enemy)
{
    WeaponRice* rice = static_cast<WeaponRice*>(weapon.weapon);

    for (auto& grain : rice->grains)
    {
        Rectangle grain_rect {
            weapon.hitbox.x + grain.position.x,
            weapon.hitbox.y + grain.position.y,
            weapon.hitbox.width,
            weapon.hitbox.height,
        };

        if (CheckCollisionRecs(enemy.hurtbox, grain_rect))
        {
            grain.should_destroy = true;
            return grain.direction;
        }

    }

    return {0, 0};
}

void UpdateWeaponRice(Weapon& weapon, const Vector2 player_position, const Vector2 closest_enemy_position, const GameTimestamps& timestamps)
{
    WeaponRice* rice = static_cast<WeaponRice*>(weapon.weapon);

    // spawn more grains if possible
    if (
        rice->grains.size() < rice->max_grains &&
        CheckCooldown(timestamps.this_update, weapon.timestamp_last_cooldown, weapon.cooldown)
    )
    {
        // create a new grain
        rice->grains.emplace_back(WeaponRice::Grain(
            player_position,
            Vector2Subtract(closest_enemy_position, player_position)
        ));

        // reset the weapon cooldown
        weapon.timestamp_last_cooldown = timestamps.this_update;
    }

    // update each grain
    std::list<WeaponRice::Grain>::iterator rice_iter;
    for (rice_iter = rice->grains.begin(); rice_iter != rice->grains.end(); rice_iter++)
    {
        // check if the grain should be destroyed
        if (rice_iter->should_destroy)
        {
            rice_iter = rice->grains.erase(rice_iter);
            continue;
        }

        // update the grain positoin
        Vector2 old_pos = rice_iter->position;
        rice_iter->position.x += rice_iter->direction.x * weapon.speed * timestamps.time_delta.count();
        rice_iter->position.y += rice_iter->direction.y * weapon.speed * timestamps.time_delta.count();
        rice_iter->distance_traveled += Vector2Distance(old_pos, rice_iter->position);

        // check if the rice grain hits
        std::list<Enemy>::iterator enemy_iter;
        bool grain_hit = false;

        // check the rice against the max distance traveled
        if (rice_iter->distance_traveled > rice->max_grain_distance)
            rice_iter->should_destroy = true;
    }
}
