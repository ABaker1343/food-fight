#ifndef __FF_HEADER_WEAPON_CORN
#define __FF_HEADER_WEAPON_CORN

#include <raylib.h>
#include <chrono>
#include <vector>

#include "weapon.hpp"
#include "../game_timestamps.hpp"

struct WeaponCorn
{
    struct Seed
    {
         Vector2 position;
         std::chrono::time_point<std::chrono::system_clock> timestamp_spawn;
         bool is_expanded = false;

         // members used by expanded seed
         float current_length = 0;
         std::vector<CollisionBox> hboxes;

         Seed(Vector2 position, std::chrono::time_point<std::chrono::system_clock> time_now);
    };

    int max_seed_count = 4;
    std::list<Seed> seeds;
    std::chrono::milliseconds expansion_time;

    WeaponCorn();
};

Weapon CreateWeaponCorn();

void UpdateWeaponCorn(Weapon& weapon, const Vector2 player_position, const GameTimestamps& timestamps);
// Get the direction that the enemy was hit returns {0, 0} if no hit
Vector2 GetWeaponHitCorn(Weapon& weapon, Enemy& enemy);

#endif
