#ifndef __FF_HEADER_WEAPON_COOKIE
#define __FF_HEADER_WEAPON_COOKIE

#include <raylib.h>

#include "../game_timestamps.hpp"
#include "weapon.hpp"

struct WeaponCookie
{
    float start_angle;
    float max_angle;
    float current_angle;
    bool alternate_side;
    CollisionBox hitbox;

    WeaponCookie();
};

Weapon CreateWeaponCookie();

void UpdateWeaponCookie(Weapon& weapon, const GameTimestamps& timestamps);
// Get the direction that the enemy was hit returns {0, 0} if no hit
Vector2 GetWeaponHitCookie(Weapon& weapon, Enemy& enemy, Vector2 player_position);

#endif
