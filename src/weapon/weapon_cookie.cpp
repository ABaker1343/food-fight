#include "weapon_cookie.hpp"

Weapon CreateWeaponCookie()
{
    Weapon new_weapon;
    new_weapon.type = WeaponType::COOKIE;

    new_weapon.damage = 100;
    new_weapon.size = {30, 130};
    new_weapon.speed = 200.f / 1000;
    new_weapon.cooldown = std::chrono::milliseconds(10);

    new_weapon.weapon = new WeaponCookie();

    return new_weapon;
}

WeaponCookie::WeaponCookie()
{
    this->start_angle = 10;
    this->max_angle = 170;
    this->current_angle = this->start_angle;
    this->alternate_side = false;
}


Vector2 GetWeaponHitCookie(Weapon& weapon, Enemy& enemy, Vector2 player_position)
{
    WeaponCookie* cookie = static_cast<WeaponCookie*>(weapon.weapon);

    Vector2 down = {0, 1};
    Vector2 down_perp = {1, 0};
    Vector2 scaled_down = Vector2Scale(down, weapon.size.y);
    Vector2 scaled_perp = Vector2Scale(down_perp, weapon.size.x);
    Vector2 cookie_vector;
    Vector2 cookie_vector_perp;

    if (cookie->alternate_side) 
    {
        cookie_vector = Vector2Rotate(scaled_down, -cookie->current_angle * DEG2RAD);
        cookie_vector_perp = Vector2Rotate(scaled_perp, -cookie->current_angle * DEG2RAD);
    }
    else
    {
        cookie_vector = Vector2Rotate(scaled_down, cookie->current_angle * DEG2RAD);
        cookie_vector_perp = Vector2Rotate(scaled_perp, cookie->current_angle * DEG2RAD);
    }

    cookie->hitbox.tl = Vector2Subtract(player_position, Vector2Scale(cookie_vector_perp, 0.5));
    cookie->hitbox.tr = Vector2Add(cookie->hitbox.tl, cookie_vector_perp);
    cookie->hitbox.bl = Vector2Add(cookie->hitbox.tl, cookie_vector);
    cookie->hitbox.br = Vector2Add(cookie->hitbox.bl, cookie_vector_perp);

    if (CheckCollision(cookie->hitbox, enemy))
    {
        return cookie_vector;
    }

    return {0, 0};
}

void UpdateWeaponCookie(Weapon& weapon, const GameTimestamps& timestamps)
{
    WeaponCookie* cookie = static_cast<WeaponCookie*>(weapon.weapon);

    // move the cookie axe (negative angle if its on the alternate side)
    cookie->current_angle += weapon.speed * timestamps.time_delta.count();
    
    // check if its done enough rotation then swap sides (also reset the rotation)
    if (cookie->current_angle > cookie->max_angle)
    {
        cookie->alternate_side = !cookie->alternate_side;
        cookie->current_angle = cookie->start_angle;
    }
}
