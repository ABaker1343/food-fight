#ifndef __FF_HEADER_WEAPON_RICE
#define __FF_HEADER_WEAPON_RICE

#include <raylib.h>

#include "../game_timestamps.hpp"
#include "weapon.hpp"

struct WeaponRice
{
    struct Grain
    {
        Vector2 position;
        Vector2 direction;
        float distance_traveled = 0;

        bool should_destroy = false;

        Grain(Vector2 staring_position, Vector2 direction);
        bool operator==(const Grain&);
    };

    float max_grain_distance = 1000;
    int max_grains = 5;
    std::list<Grain> grains;

    WeaponRice();
};

Weapon CreateWeaponRice();

void UpdateWeaponRice(Weapon& weapon, const Vector2 player_position, const Vector2 closes_enemy_position, const GameTimestamps& timestamps);
// Get the direction that the enemy was hit returns {0, 0} if no hit
Vector2 GetWeaponHitRice(Weapon& weapon, Enemy& enemy);

#endif
