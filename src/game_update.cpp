#include "entity/enemy.hpp"
#include "entity/player.hpp"
#include "game.hpp"

void UpdateGame(GameState& state)
{
    UpdatePlayer(state);
    UpdateCamera(state);
    UpdateStage(state);
    UpdateEnemies(state);
    UpdateWeapons(state);
}

void UpdateCamera(GameState& state)
{
    state.camera.target.x = state.player.position.x - (static_cast<float>(GetRenderWidth()) / 2) + (state.player.scale / 2);
    state.camera.target.y = state.player.position.y - (static_cast<float>(GetRenderHeight()) / 2) + (state.player.scale / 2);
}

void UpdatePlayer(GameState& state)
{
    UpdatePlayer(state.player, state.timestamps);

    if (state.player.is_dead)
    {
        EndGame(state, GameEndCondition::PLAYER_DEAD);
    }

    UpdateAnimatedSprite(state.assets.player_sprite, state.timestamps.this_update);
}

void UpdateStage(GameState& state)
{
    // check if we have to move the stages
    UpdateStage(state.stage, GetPlayerCenter(state.player));
}

void UpdateEnemies(GameState& state)
{
    // reset the closest enemy
    state.closest_enemy = nullptr;
    state.closest_enemy_distance = std::numeric_limits<float>::max();

    if (state.enemies.size() < state.max_enemy_count)
    {
        int rand = (EnemyType)(std::rand() % 10);
        if (rand == 0) SpawnEnemy(state, EnemyType::KAREN);
        else SpawnEnemy(state, EnemyType::CUSTOMER);
    }

    std::list<Enemy>::iterator enemy_iter;
    for (enemy_iter = state.enemies.begin(); enemy_iter != state.enemies.end(); enemy_iter++)
    {
        switch(enemy_iter->type)
        {
            case EnemyType::CUSTOMER:
            {
                UpdateEnemyCustomer(*enemy_iter, state.player, state.timestamps);
                break;
            }
            case EnemyType::KAREN:
            {
                UpdateEnemyKaren(*enemy_iter, state.player, state.timestamps);
                break;
            }
        }

        float enemy_distance = Vector2Distance(enemy_iter->position, state.player.position);
        if (enemy_distance < state.closest_enemy_distance) {
            state.closest_enemy_distance = enemy_distance;
            state.closest_enemy = &*enemy_iter;
        }

        // if enemy is too far away despawn it
        if (enemy_distance > state.enemy_despawn_distance)
        {
            enemy_iter = state.enemies.erase(enemy_iter);
            continue;
        }
    }
}

void UpdateWeapons(GameState& state)
{
    for(auto& weapon : state.player.weapons)
    {
        switch(weapon.type)
        {
            case RICE:
            {
                UpdateWeaponRice(
                    weapon,
                    GetPlayerCenter(state.player),
                    GetEnemyCenter(*state.closest_enemy),
                    state.timestamps
                );
                std::list<Enemy>::iterator enemy_iter;
                for (enemy_iter = state.enemies.begin(); enemy_iter != state.enemies.end(); enemy_iter++)
                {
                    Vector2 hit_direction = GetWeaponHitRice(weapon, *enemy_iter);
                    if (hit_direction != Vector2{0, 0})
                    {
                        if (DamageEnemy(state, enemy_iter, weapon.damage))
                            continue; // continue if enemy was killed

                        ApplyKnockback(*enemy_iter, hit_direction, 10);
                    }
                }
                break;
            }
            case CORN:
            {
                UpdateWeaponCorn(
                    weapon,
                    GetPlayerCenter(state.player),
                    state.timestamps
                );
                std::list<Enemy>::iterator enemy_iter;
                for (enemy_iter = state.enemies.begin(); enemy_iter != state.enemies.end(); enemy_iter++)
                {
                    Vector2 hit_direction = GetWeaponHitCorn(weapon, *enemy_iter);
                    if (hit_direction != Vector2{0, 0})
                    {
                        if (DamageEnemy(state, enemy_iter, weapon.damage))
                            continue;

                        ApplyKnockback(*enemy_iter, hit_direction, 10);
                    }
                }
                break;
            }
            case COOKIE:
            {
                UpdateWeaponCookie(
                    weapon,
                    state.timestamps
                );
                std::list<Enemy>::iterator enemy_iter;
                for (enemy_iter = state.enemies.begin(); enemy_iter != state.enemies.end(); enemy_iter++)
                {
                    Vector2 hit_direction = GetWeaponHitCookie(weapon, *enemy_iter, GetPlayerCenter(state.player));
                    if (hit_direction != Vector2{0, 0})
                    {
                        if (DamageEnemy(state, enemy_iter, weapon.damage))
                            continue;

                        ApplyKnockback(*enemy_iter, hit_direction, 10);
                    }
                }
                break;
            }
            default:
                break;
        }
    }
}

void UpdateExp(GameState& state, int amount)
{
    state.player.current_exp += amount;
    while (state.player.current_exp > state.player.exp_to_next_level)
    {
        // increase player level
        state.player.level += 1;

        // update the next exp milestone
        state.player.exp_to_prev_level = state.player.exp_to_next_level;
        state.player.exp_to_next_level += state.player.exp_to_next_level / 2;
    }
}
