#include "entity/enemy_karen.hpp"
#include "game.hpp"

void RenderGameState(GameState& state)
{
    BeginDrawing();
        ClearBackground(BLACK);

        BeginMode2D(state.camera);

            RenderStage(state);
            RenderPlayer(state);
            RenderEnemies(state);
            RenderWeapons(state);

        EndMode2D();

    RenderUI(state);
    RenderDebugInfo(state);

    EndDrawing();

}

void RenderStage(GameState& state)
{
    Stage& stage = state.stage;
    for (const auto& chunk : stage.chunks)
    {
        for (int y = 0; y < chunk.tile_grid.size(); y++)
        {
            const std::vector<Tile>& row = chunk.tile_grid[y];
            for (int x = 0; x < row.size(); x++)
            {
                const Tile& tile = row[x];

                switch(tile.type)
                {
                    case TileType::OPEN:
                    {
                        DrawRectangle(
                            chunk.position.x + x * stage.tile_size, 
                            chunk.position.y + y * stage.tile_size,
                            stage.tile_size, stage.tile_size,
                            LIGHTGRAY
                        );
                        break;
                    }
                    case TileType::WALL:
                    {
                        DrawRectangle(
                            chunk.position.x + x * stage.tile_size, 
                            chunk.position.y + y * stage.tile_size,
                            stage.tile_size, stage.tile_size,
                            GRAY
                        );
                        break;
                    }
                    default:
                        break;
                }
            }
        }
    }
}

void RenderPlayer(GameState& state)
{
    AnimatedSprite& sprite = state.assets.player_sprite;
    Player& p = state.player;

    // draw player sprite
    DrawTexturePro(
        state.assets.player_sprite.sprite,
        sprite.frame_src, // src
        Rectangle{p.position.x, p.position.y, p.scale, p.scale}, // dest
        Vector2{0,0}, // origin
        0, // rotation
        WHITE // tint
    );


    // draw health bar
    DrawRectangle(
        p.position.x, p.position.y + p.scale,
        p.scale * (p.health / p.max_health), 10, 
        RED
    );
}

void RenderEnemies(GameState& state)
{
    for (const auto& enemy : state.enemies)
    {
        switch(enemy.type) {
            case CUSTOMER: {
                DrawRectangleV(
                    enemy.position,
                    {enemy.size, enemy.size},
                    RED
                );
                break;
            }
            case KAREN: {
                EnemyKaren* karen = static_cast<EnemyKaren*>(enemy.enemy);
                DrawRectangleV(
                    enemy.position,
                    {enemy.size, enemy.size},
                    PURPLE
                );
                for (const auto& p : karen->projectiles)
                {
                    DrawRectangleV(
                        p.position,
                        p.size,
                        PURPLE
                    );
                }
                break;
            }

        }
    }
}

void RenderWeapons(GameState& state)
{
    for (auto& weapon : state.player.weapons)
    {
        switch(weapon.type)
        {
            case RICE:
            {
                RenderWeaponRice(state, weapon);
                break;
            }
            case CORN:
            {
                RenderWeaponCorn(state, weapon);
                break;
            }
            case COOKIE:
            {
                RenderWeaponCookie(state, weapon);
            }
        }
    }
}

void RenderWeaponRice(GameState& state, Weapon& weapon)
{
    WeaponRice* rice = static_cast<WeaponRice*>(weapon.weapon);
    for (const auto& grain : rice->grains)
    {
        DrawRectangleV(
            grain.position,
            weapon.size,
            WHITE
        );
    }
}

void RenderWeaponCorn(GameState& state, Weapon& weapon)
{
    WeaponCorn* corn = static_cast<WeaponCorn*>(weapon.weapon);
    for (const auto& seed : corn->seeds)
    {
        if (seed.is_expanded)
        {
            float angle = 0;
            for (const auto& box : seed.hboxes)
            {
                Rectangle rec;
                rec.x = seed.position.x;
                rec.y = seed.position.y;
                rec.width = weapon.size.x;
                rec.height = seed.current_length;

                DrawRectanglePro(
                    //rec, {0 + weapon.size.x / 2 ,0}, angle, DARKGREEN
                    rec, {0,0}, angle, DARKGREEN
                );

                angle += (360.f / seed.hboxes.size());


                DrawRectangle(box.tl.x - 2, box.tl.y -2, 4, 4, BLUE);
                DrawRectangle(box.tr.x - 2, box.tr.y -2, 4, 4, BLUE);
                DrawRectangle(box.bl.x - 2, box.bl.y -2, 4, 4, BLUE);
                DrawRectangle(box.br.x - 2, box.br.y -2, 4, 4, BLUE);
            }
        } else
        {
            // if its no expanded then draw the size of the thickness
            DrawRectangleV(
                seed.position,
                {weapon.size.x, weapon.size.x},
                GREEN
            );
        }
    }
}

void RenderWeaponCookie(GameState& state, Weapon& weapon)
{
    WeaponCookie* cookie = static_cast<WeaponCookie*>(weapon.weapon);
    float angle;

    if (cookie->alternate_side)
        angle = -cookie->current_angle;
    else
        angle = cookie->current_angle;

    Vector2 player_center = GetPlayerCenter(state.player);

    DrawRectanglePro(
        {player_center.x, player_center.y, weapon.size.x, weapon.size.y},
        {weapon.size.x / 2, 0},
        angle,
        BROWN
    );

    CollisionBox box = cookie->hitbox;
    DrawRectangle(box.tl.x - 2, box.tl.y -2, 4, 4, BLUE);
    DrawRectangle(box.tr.x - 2, box.tr.y -2, 4, 4, BLUE);
    DrawRectangle(box.bl.x - 2, box.bl.y -2, 4, 4, BLUE);
    DrawRectangle(box.br.x - 2, box.br.y -2, 4, 4, BLUE);
}

void RenderUI(GameState& state)
{
    // render the kill count and exp
    int ui_text_size = 32;
    int spacing = 8;
    int ui_offset = ui_text_size + spacing;

    // draw exp bar
    float bar_percentage = 
        static_cast<float>(state.player.current_exp - state.player.exp_to_prev_level)
        /
        static_cast<float>(state.player.exp_to_next_level - state.player.exp_to_prev_level);

    DrawRectangle(
        0, 0,
        bar_percentage * GetRenderWidth(),
        ui_text_size,
        BLUE
    );

    // draw the kill count
    std::string kill_count_str = "Kills: " + std::to_string(state.player.kill_count);
    DrawText(
        kill_count_str.c_str(),
        0, ui_offset, 32, WHITE
    );

    // draw the exp count
    std::string exp_str = "Exp: " + std::to_string(state.player.current_exp);
    DrawText(
        exp_str.c_str(),
        0, ui_offset * 2, 32, WHITE
    );
}

void RenderDebugInfo(GameState& state)
{
    int text_size = 32;
    Color text_color = WHITE;

    std::string player_pos_string =
        "player position: " +
        std::to_string(state.player.position.x) + ", " +
        std::to_string(state.player.position.y);

    std::string player_exp_string = 
        "Player exp: " + std::to_string(state.player.current_exp) + " / " + std::to_string(state.player.exp_to_next_level);

    std::string level_exp_string =
        "exp to prev|next: " + std::to_string(state.player.exp_to_prev_level) + " | " + std::to_string(state.player.exp_to_next_level);

    DrawText(player_pos_string.c_str(), 0, GetRenderHeight() - text_size, text_size, text_color);
    DrawText(player_exp_string.c_str(), 0, GetRenderHeight() - text_size * 2, text_size, text_color);
    DrawText(level_exp_string.c_str(), 0, GetRenderHeight() - text_size * 3, text_size, text_color);

    // draw frame fps
    DrawFPS(GetRenderWidth() - 32, 0);
    
}
