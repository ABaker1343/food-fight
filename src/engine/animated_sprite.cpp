#include "animated_sprite.hpp"

AnimatedSprite::AnimatedSprite(std::string filepath, int num_frames, float animation_speed)
{
    this->sprite = LoadTexture(filepath.c_str());
    this->num_frames = num_frames;
    this->animation_speed = 1 / (animation_speed / 1000); // convert frames per second to milliseconds per frame

    this->current_frame = 0;

    this->frame_src.x = 0;
    this->frame_src.y = 0;
    this->frame_src.width = static_cast<float>(sprite.width) / num_frames;
    this->frame_src.height = sprite.height;
}

AnimatedSprite::~AnimatedSprite()
{
    UnloadTexture(this->sprite);
}

void UpdateAnimatedSprite(AnimatedSprite& sprite, std::chrono::time_point<std::chrono::system_clock> time_now)
{
    auto time_delta = std::chrono::duration_cast<std::chrono::milliseconds>(
        time_now - sprite.timestamp_last_frame
    );

    if (time_delta.count() > sprite.animation_speed)
    {
        sprite.current_frame++;
        if (sprite.current_frame >= sprite.num_frames)
        {
            sprite.current_frame = 0;
        }

        sprite.frame_src.x = sprite.current_frame * sprite.frame_src.width;

        sprite.timestamp_last_frame = time_now;
    }
}
