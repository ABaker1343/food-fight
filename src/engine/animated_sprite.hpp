#ifndef __FF_HEADER_ANIMATED_SPRITE
#define __FF_HEADER_ANIMATED_SPRITE

#include <chrono>
#include <string>
#include <raylib.h>

struct AnimatedSprite
{
    Texture2D sprite;
    float animation_speed;
    int num_frames;

    int current_frame;
    Rectangle frame_src;

    std::chrono::time_point<std::chrono::system_clock> timestamp_last_frame;

    AnimatedSprite(std::string filepath, int num_frames, float animation_speed);
    ~AnimatedSprite();

    AnimatedSprite(AnimatedSprite&) = delete;
};

void UpdateAnimatedSprite(AnimatedSprite& sprite, std::chrono::time_point<std::chrono::system_clock> time_now);

#endif
