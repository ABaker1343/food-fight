#ifndef __FF_HEADER_RAYLIB_OPERATOR
#define __FF_HEADER_RAYLIB_OPERATOR

#include <raylib.h>

bool operator==(const Vector2& lhs, const Vector2& rhs);

bool operator!=(const Vector2& lhs, const Vector2& rhs);

#endif
