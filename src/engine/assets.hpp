#ifndef __FF_HEADER_ASSETS
#define __FF_HEADER_ASSETS

#include <raylib.h>

#include "animated_sprite.hpp"

struct GameAssets
{
    int player_animation_speed = 2 / 1000; // frames per millisecond

    AnimatedSprite player_sprite;

    GameAssets();
    ~GameAssets();
    GameAssets(GameAssets&) = delete;
};


#endif
