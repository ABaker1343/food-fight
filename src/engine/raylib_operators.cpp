#include "raylib_operators.hpp"

bool operator==(const Vector2& lhs, const Vector2& rhs)
{
    return lhs.x == rhs.x && lhs.y == rhs.y;
}

bool operator!=(const Vector2& lhs, const Vector2& rhs)
{
    return !(lhs == rhs);
}
