#include "game.hpp"
#include "entity/enemy_karen.hpp"
#include "weapon/weapon.hpp"
#include <iterator>
#include <limits>
#include <raylib.h>
#include <raymath.h>
#include <thread>

GameState::GameState()
{
    this->camera.zoom = 1;
    this->camera.offset = {0, 0};
    this->camera.target = {player.position};
    this->camera.rotation = 0;

    std::srand(std::time(NULL));
}

void RunGame(GameState& state)
{

    state.player.weapons.emplace_back(CreateWeaponRice());
    state.player.weapons.emplace_back(CreateWeaponCorn());
    state.player.weapons.emplace_back(CreateWeaponCookie());

    // initial timestamp
    state.timestamps.last_update = std::chrono::system_clock::now();

    state.is_running = true;
    while (state.is_running && !WindowShouldClose())
    {
        // update timestamps
        auto time_now = std::chrono::system_clock::now();

        state.timestamps.time_delta = std::chrono::duration_cast<std::chrono::milliseconds>(
            time_now - state.timestamps.last_update
        );
        state.timestamps.this_update = time_now;

        UpdateGame(state);
        RenderGameState(state);

        // delay updating last update until updates are finished
        state.timestamps.last_update = time_now;
    }
}

void EndGame(GameState& state, GameEndCondition condition)
{
    switch(condition)
    {
        case GameEndCondition::PLAYER_DEAD:
        {
            std::string game_over_text = "Game Over";
            BeginDrawing();
            DrawText(
                game_over_text.c_str(), 
                (GetRenderWidth() / 2) - (32 * (game_over_text.length() / 2)),
                GetRenderHeight() / 2 - 32,
                64,
                RED
            );
            EndDrawing();
            WaitTime(5);
            break;
        }
    }

    state.is_running = false;
}

bool DamageEnemy(GameState& state, std::list<Enemy>::iterator& enemy_iterator, float damage)
{
    enemy_iterator->health -= damage;
    
    if (enemy_iterator->health <=0 )
    {
        UpdateExp(state, enemy_iterator->exp);
        state.player.kill_count += 1;
        enemy_iterator = state.enemies.erase(enemy_iterator);
        // enemy was killed
        return true;
    }

    // enemy was not killed
    return false;
}

void SpawnEnemy(GameState& state, EnemyType type)
{
    static const float spawn_distance = 1000;

    switch(type)
    {
        case CUSTOMER:
            state.enemies.emplace_back(CreateEnemyCustomer(state.player.level));
            break;
        case KAREN:
            state.enemies.emplace_back(CreateEnemyKaren(state.player.level));
            break;
        default:
            break;
    }

    Enemy& new_enemy = state.enemies.back();
    
    // generaate 2 random numbers between 0 and 1;
    float rand_x = static_cast<float>(std::rand() / static_cast<float>(RAND_MAX));
    float rand_y = static_cast<float>(std::rand() / static_cast<float>(RAND_MAX));

    // make the random numbers between -1 and 1
    rand_x = (rand_x * 2) - 1;
    rand_y = (rand_y * 2) - 1;
    
    // use the numbers to create a direction vector for where the enemy will spawn
    Vector2 spawn_dir = {rand_x, rand_y};

    spawn_dir = Vector2ChangeLength(spawn_dir, spawn_distance);

    new_enemy.position = Vector2Add(state.player.position, spawn_dir);
}
