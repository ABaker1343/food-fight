#include "stage.hpp"

Stage::Stage()
{
    this->tile_size = 50;
    this->chunk_size = chunks[NEUTRAL].tile_grid.size() * tile_size;

    this->chunks[HORIZONTAL] = this->chunks[NEUTRAL];
    this->chunks[VERTICAL] = this->chunks[NEUTRAL];
    this->chunks[DIAGANOL] = this->chunks[NEUTRAL];
    
    this->chunks[NEUTRAL].position = {-this->chunk_size / 2, -this->chunk_size / 2};
    this->chunks[HORIZONTAL].position = Vector2Subtract(this->chunks[NEUTRAL].position, {this->chunk_size, 0});
    this->chunks[VERTICAL].position = Vector2Subtract(this->chunks[NEUTRAL].position, {0, this->chunk_size});
    this->chunks[DIAGANOL].position = Vector2Subtract(this->chunks[NEUTRAL].position, {this->chunk_size, this->chunk_size});
}

Stage::Chunk::Chunk()
{
    int num_tiles = 50;
    this->tile_grid.resize(num_tiles);
    this->position = {0, 0};

    for (auto& row : this->tile_grid)
    {
        row.resize(num_tiles);
        for (auto& tile : row) 
        {
            int random = std::rand() % 2;
            tile.type = (TileType)random;
        }
    }
}

void UpdateStage(Stage& stage, const Vector2 player_position)
{
    auto& chunks = stage.chunks;

    // if the player crosses the halfway boundary on the neutral stage then move the horizontal and vertical chunks in that direction
    
    // player moving right
    if (player_position.x > chunks[NEUTRAL].position.x + 0.5 * stage.chunk_size)
    {
        chunks[HORIZONTAL].position = Vector2Add(chunks[NEUTRAL].position, {stage.chunk_size, 0});
        // player is going over the halfway or the next stage part 
        if (player_position.x > chunks[HORIZONTAL].position.x + 0.5 * stage.chunk_size)
        {
            chunks[NEUTRAL].position = chunks[HORIZONTAL].position;
            chunks[HORIZONTAL].position = Vector2Add(chunks[NEUTRAL].position, {stage.chunk_size, 0});
            //chunks[VERTICAL].position = Vector2Add(chunks[NEUTRAL].position, {0, stage.chunk_size});
        }
    }

    // player moving left
    if (player_position.x < chunks[NEUTRAL].position.x + 0.5 * stage.chunk_size)
    {
        chunks[HORIZONTAL].position = Vector2Subtract(chunks[NEUTRAL].position, {stage.chunk_size, 0});
        if (player_position.x < chunks[HORIZONTAL].position.x + 0.5 * stage.chunk_size)
        {
            chunks[NEUTRAL].position = chunks[HORIZONTAL].position;
            chunks[HORIZONTAL].position = Vector2Subtract(chunks[NEUTRAL].position, {stage.chunk_size, 0});
            //chunks[VERTICAL].position = Vector2Add(chunks[NEUTRAL].position, {0, stage.chunk_size});
        }
    }
    
    // player moving up
    if (player_position.y < chunks[NEUTRAL].position.y + 0.5 * stage.chunk_size)
    {
        chunks[VERTICAL].position = Vector2Subtract(chunks[NEUTRAL].position, {0, stage.chunk_size});
        if (player_position.y < chunks[VERTICAL].position.y + 0.5 * stage.chunk_size)
        {
            chunks[NEUTRAL].position = chunks[VERTICAL].position;
            chunks[VERTICAL].position = Vector2Subtract(chunks[NEUTRAL].position, {stage.chunk_size, 0});
            //chunks[HORIZONTAL].position = Vector2Subtract(chunks[NEUTRAL].position, {stage.chunk_size, 0});
        }
    }

    // player moving down
    if (player_position.y > chunks[NEUTRAL].position.y + 0.5 * stage.chunk_size)
    {
        chunks[VERTICAL].position = Vector2Add(chunks[NEUTRAL].position, {0, stage.chunk_size});
        if (player_position.y > chunks[VERTICAL].position.y + 0.5 * stage.chunk_size)
        {
            chunks[NEUTRAL].position = chunks[VERTICAL].position;
            chunks[VERTICAL].position = Vector2Add(chunks[NEUTRAL].position, {stage.chunk_size, 0});
            //chunks[HORIZONTAL].position = Vector2Subtract(chunks[NEUTRAL].position, {stage.chunk_size, 0});
        }
    }

    chunks[DIAGANOL].position.x = chunks[HORIZONTAL].position.x;
    chunks[DIAGANOL].position.y = chunks[VERTICAL].position.y;

}
