#ifndef __FF_HEADER_STAGE
#define __FF_HEADER_STAGE

#include <vector>
#include <array>
#include <cstdlib>
#include <raylib.h>
#include <raymath.h>

enum StageType
{
    BBQ,
};

enum TileType
{
    OPEN = 0,
    WALL = 1,
};

enum StageDirections
{
    NEUTRAL = 0,
    VERTICAL = 1,
    HORIZONTAL = 2,
    DIAGANOL = 3,
};

struct Tile
{
    TileType type;
};

struct Stage
{
    struct Chunk
    {
        std::vector<std::vector<Tile>> tile_grid;

        Vector2 position;

        Chunk();
    };

    StageType type;
    float tile_size;
    float chunk_size;

    std::array<Chunk, 4> chunks;

    Stage();
};

void UpdateStage(Stage& stage, const Vector2 player_position);

#endif
