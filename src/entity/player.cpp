#include "player.hpp"

Player::Player()
{
    this->hurtbox = Rectangle{0, 0, scale, scale};
}

void UpdatePlayer(Player& player, const GameTimestamps& timestamps)
{
    if (player.health <= 0)
    {
        player.is_dead = true;
        return;
    }

    // check if health regen cooldown is done
    if (CheckCooldown(timestamps.this_update, player.timestamp_last_damage, timestamps.time_delta)) {
        player.health = std::min(
            player.max_health, 
            player.health + (player.health_regen_rate * timestamps.time_delta.count())
        );
    }

    // update position
    if (IsKeyDown(KEY_A))
        player.position.x -= player.movement_speed * timestamps.time_delta.count();
    if (IsKeyDown(KEY_D))
        player.position.x += player.movement_speed * timestamps.time_delta.count();
    if (IsKeyDown(KEY_S))
        player.position.y += player.movement_speed * timestamps.time_delta.count();
    if (IsKeyDown(KEY_W))
        player.position.y -= player.movement_speed * timestamps.time_delta.count();

    player.hurtbox.x = player.position.x;
    player.hurtbox.y = player.position.y;
}

void DamagePlayer(Player& player, float damage, std::chrono::time_point<std::chrono::system_clock> time_now)
{
    player.health -= damage;
    player.timestamp_last_damage = time_now;
}
