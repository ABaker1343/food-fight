#ifndef __FF_HEADER_ENEMY_KAREN
#define __FF_HEADER_ENEMY_KAREN

#include <raylib.h>
#include <raymath.h>

#include "enemy.hpp"
#include "player.hpp"
#include "../game_timestamps.hpp"

struct EnemyKaren
{
    struct Projectile {
        Vector2 position;
        Vector2 direction;
        Vector2 size;
        float distance_traveled;
        
        Projectile(Vector2 start_position, Vector2 direction);
    };

    std::list<Projectile> projectiles;
    float max_projectiles;
    std::chrono::milliseconds projectile_cooldown;
    std::chrono::time_point<std::chrono::system_clock> timestamp_last_projectile;

    float projectile_speed;
    float max_projectile_distance;
    float desired_distance;

    EnemyKaren();
};

Enemy CreateEnemyKaren(uint level);

void UpdateEnemyKaren(Enemy& enemy, Player& player, const GameTimestamps& timestamps);

#endif
