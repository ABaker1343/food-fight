#include "enemy_customer.hpp"

Enemy CreateEnemyCustomer(uint level)
{
    Enemy new_enemy;
    new_enemy.type = EnemyType::CUSTOMER;

    new_enemy.health = 100 * level * 1.10;
    new_enemy.damage = 30;
    new_enemy.speed = 2.f / 1000 * 60;
    new_enemy.size = 50;
    new_enemy.exp = 10;

    new_enemy.hurtbox = {0, 0, new_enemy.size, new_enemy.size};

    new_enemy.enemy = new EnemyCustomer();

    return new_enemy;
}

void UpdateEnemyCustomer(Enemy& enemy, Player& player, const GameTimestamps& timestamps)
{
    // update update the enemies position
    Vector2 player_direction = Vector2Normalize(Vector2Subtract(player.position, enemy.position));

    Vector2 movement = Vector2Scale(player_direction, enemy.speed * timestamps.time_delta.count());
    enemy.position.x += movement.x;
    enemy.position.y += movement.y;

    // update hitbox
    enemy.hurtbox.x = enemy.position.x;
    enemy.hurtbox.y = enemy.position.y;

    // check if its the closest enemy
    float distance_from_player = Vector2Distance(player.position, enemy.position);

    // check for colision with the player
    if (CheckCollisionRecs(player.hurtbox, enemy.hurtbox))
    {
        if (CheckCooldown(timestamps.this_update, enemy.timestamp_last_damage_deal, enemy.damage_cooldown))
        {
            player.health -= enemy.damage;
            player.timestamp_last_damage = timestamps.this_update;
            enemy.timestamp_last_damage_deal = timestamps.this_update;
        }
    }
}
