#include "enemy_karen.hpp"
#include <raymath.h>

Enemy CreateEnemyKaren(uint level)
{
    Enemy new_enemy;
    new_enemy.type = EnemyType::KAREN;

    new_enemy.health = 80 * level * 1.1;
    new_enemy.damage = 15;
    new_enemy.speed = 1.f / 1000 * 60;
    new_enemy.size = 50;
    new_enemy.exp = 10;

    new_enemy.hurtbox = {0, 0, new_enemy.size, new_enemy.size};

    new_enemy.enemy = new EnemyKaren();

    return new_enemy;
}

EnemyKaren::EnemyKaren()
{
    this->timestamp_last_projectile = std::chrono::system_clock::now();
    this->projectile_cooldown = std::chrono::milliseconds(800);
    this->max_projectiles = 2;
    this->projectile_speed = 10.f / 1000 * 60;
    this->max_projectile_distance = 1000;
    this->desired_distance = 400;
}

EnemyKaren::Projectile::Projectile(Vector2 start_position, Vector2 direction)
{
    this->direction = Vector2Normalize(direction);
    this->position = start_position;
    this->size = {25, 25};
    this->distance_traveled = 0;
}

void UpdateEnemyKaren(Enemy& enemy, Player& player, const GameTimestamps& timestamps)
{
    EnemyKaren* karen = static_cast<EnemyKaren*>(enemy.enemy);

    Vector2 player_direction = Vector2Normalize(Vector2Subtract(player.position, enemy.position));
    Vector2 movement = Vector2Scale(player_direction, enemy.speed * timestamps.time_delta.count());

    float new_distance = Vector2Distance(player.position, Vector2Add(enemy.position, movement));

    // if too far away move closer
    if (new_distance > karen->desired_distance)
    {
        enemy.position = Vector2Add(enemy.position, movement);
    } else // if too close move away
    {
        enemy.position = Vector2Subtract(enemy.position, movement);
    }

    // update hitbox
    enemy.hurtbox.x = enemy.position.x;
    enemy.hurtbox.y = enemy.position.y;

    
    // add new projectiles
    if (karen->projectiles.size() < karen->max_projectiles)
    {
        karen->projectiles.emplace_back(EnemyKaren::Projectile(GetEnemyCenter(enemy), player_direction));
    }

    // update existing projectiles
    std::list<EnemyKaren::Projectile>::iterator proj_iter;
    for (proj_iter = karen->projectiles.begin(); proj_iter != karen->projectiles.end(); proj_iter++)
    {
        // move the projectile
        Vector2 movement = Vector2Scale(proj_iter->direction, karen->projectile_speed * timestamps.time_delta.count());
        proj_iter->position = Vector2Add(proj_iter->position, movement);
        proj_iter->distance_traveled += Vector2Length(movement);

        Rectangle proj_hitbox = {
            proj_iter->position.x, proj_iter->position.y,
            proj_iter->size.x, proj_iter->size.y,
        };

        // check for hits
        if (CheckCollisionRecs(proj_hitbox, player.hurtbox))
        {
            if (CheckCooldown(timestamps.this_update, player.timestamp_last_damage, player.damage_grace_period))
            {
                // damage the player
                DamagePlayer(player, enemy.damage, timestamps.this_update);
                enemy.timestamp_last_damage_deal = timestamps.this_update;

                // delete the projectile and move to the next
                proj_iter = karen->projectiles.erase(proj_iter);
                continue;
            }
        }

        // if the projectile has gone too far remove it
        if (Vector2Distance(proj_iter->position, player.position) > karen->max_projectile_distance)
        {
            proj_iter = karen->projectiles.erase(proj_iter);
            continue;
        }
    }
}
