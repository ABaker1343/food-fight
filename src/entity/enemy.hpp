#ifndef __FF_HEADER_ENEMY
#define __FF_HEADER_ENEMY

#include <chrono>
#include <cstdlib>
#include <raylib.h>
#include <raymath.h>

#include "../game_timestamps.hpp"

enum EnemyType
{
    CUSTOMER,
    KAREN, // ranged customer, always returning food
};

struct Enemy
{
    EnemyType type; 
    Vector2 position;
    float health = 100;

    float damage = 5;
    float speed = (2.f / 1000) * 60; // in unit / milliseconds
    float size = 50;

    int exp = 10;

    Rectangle hurtbox;
    std::chrono::time_point<std::chrono::system_clock> timestamp_last_damage_deal;
    std::chrono::milliseconds damage_cooldown = std::chrono::milliseconds(500);

    void* enemy = nullptr;

    Enemy();
    Enemy(Enemy&) = delete;
    Enemy(Enemy&&);
    ~Enemy();
};

inline Vector2 GetEnemyCenter(Enemy& enemy)
{
    float half_size = enemy.size / 2;
    return {enemy.position.x + half_size, enemy.position.y + half_size};
}

void ApplyKnockback(Enemy& enemy, Vector2 direction, float amount);


#endif
