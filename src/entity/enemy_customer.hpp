#ifndef __FF_HEADER_ENEMY_CUSTOMER
#define __FF_HEADER_ENEMY_CUSTOMER

#include "enemy.hpp"
#include "player.hpp"

struct EnemyCustomer
{

};

Enemy CreateEnemyCustomer(uint level);

void UpdateEnemyCustomer(Enemy& enemy, Player& player, const GameTimestamps& timestamps);

#endif
