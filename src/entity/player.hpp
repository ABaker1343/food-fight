#ifndef __FF_HEADER_PLAYER
#define __FF_HEADER_PLAYER

#include <vector>
#include <chrono>
#include <raylib.h>

#include "../weapon/weapon.hpp"
#include "../game_timestamps.hpp"

enum PlayerState
{
    IDLE
};

struct Player
{
    PlayerState state = IDLE;
    Vector2 position = {0, 0};
    float movement_speed = 6.0f / 1000 * 60; // unit / milliseconds
    float scale = 100.f;
    Rectangle hurtbox;

    std::vector<Weapon> weapons;
    float max_health = 100;
    float health = 100;
    bool is_dead = false;

    std::chrono::time_point<std::chrono::system_clock> timestamp_last_damage;
    std::chrono::milliseconds damage_grace_period = std::chrono::milliseconds(200);
    std::chrono::milliseconds health_regen_cooldown = std::chrono::milliseconds(3000);
    float health_regen_rate = 10.f / 1000; // milliseconds
    
    uint kill_count = 0;
    uint level = 1;
    uint current_exp = 0;
    uint exp_to_prev_level = 0;
    uint exp_to_next_level = 100;

    Player();
};

inline Vector2 GetPlayerCenter(Player& player)
{
    float half_size = player.scale / 2;
    return {player.position.x + half_size, player.position.y + half_size};
}

void UpdatePlayer(Player& player, const GameTimestamps& timestamps);

void DamagePlayer(Player& player, float damage, std::chrono::time_point<std::chrono::system_clock> time_now);

#endif
