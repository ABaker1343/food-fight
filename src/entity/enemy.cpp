#include "enemy.hpp"

Enemy::Enemy()
{
    this->timestamp_last_damage_deal = std::chrono::system_clock::now();
    this->damage_cooldown = std::chrono::milliseconds(500);
}

Enemy::Enemy(Enemy&& src) {
    this->type = src.type;
    this->health = src.health;
    this->damage = src.damage;
    this->speed = src.speed;
    this->size = src.size;
    this->exp = src.exp;
    this->hurtbox = src.hurtbox;
    this->timestamp_last_damage_deal = src.timestamp_last_damage_deal;
    this->damage_cooldown = src.damage_cooldown;

    this->enemy = src.enemy;
    src.enemy = nullptr;
}

Enemy::~Enemy() {
    if (this->enemy != nullptr)
        std::free(enemy);
}

void ApplyKnockback(Enemy& enemy, Vector2 direction, float amount)
{
    direction = Vector2Normalize(direction);
    Vector2 knockback_vector = Vector2Scale(direction, amount);

    enemy.position = Vector2Add(enemy.position, knockback_vector);
}
