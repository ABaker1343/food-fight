# Food Fight TODO

# Gameplay Features

## Enemies

  ### Regular Customers
  Regular grunt like customers that walk at you and deal damage when they touch you

  ### Gluttonous Customers
  Customers that are larger tankier versions of [[#Regular Customers]]

  ### Karen Customers
  Customers that violently return their food via projectile

## Critics

## Base Weapons

  ### Rice
  Projectile weapon that fires small projectiles at the closest enemy

  ### Banana
  Boomerang like weapon?

  ### Potato

  ### Pasta

  ### Corn
  Plants seeds in the ground that exploed and send vines along the ground damaging enemies on hit

## Upgraded Weapons

  ### Sou Chef
  Sou Checf that will perodically join the battle and perform some function

  ### Spaghetti Whip
  Combination of spaghetti with meatballs on the end, swing in a circle damaging enemies at the end of its radius

  ### Cookie Axe
  Melee weapon that swings in half circles alternating the side

  ### ???
  Sends Streaks horizontally across the screen dealing damage to anything it touches, going left to right vertically offset

## Spices

## Cooking/Recipies

# Stages

## BBQ
  Stage that takes place at a garden BBQ

## Restaurant
  Stage that takes place at a restaurant

# Flavor/Flair

## Shop Keeper/Supplier
  some sort of shop keeper/supplier on the upgrade screen that gives you your new weapons/ingredients

## Borderlands Style Boss Intros
  Borderlands style freeze frame boss intros with higher res spries and custom hand drawn font with their name
